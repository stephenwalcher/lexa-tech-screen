// Import Lodash package
const _ = require("lodash");

// Instantiate/declare variables
let data,
    expired = [],
    nonExpired = [],
    formattedData;

// Import required data
try {
    data = require("./q1data.json");

} catch (error) {
    console.error("Could not find required data file!");
    process.exit(1);
}

// If the data exists, look through all the items
_.forEach(data, (item) => {
    if (item.isExpired) {
        // Add to expired array if marked as expired
        expired.push(item);

    } else {
        // Add to nonExpired array if not marked as expired
        nonExpired.push(item);
    }
});

// Concatenate the two arrays together, with nonExpired first
formattedData = nonExpired.concat(expired);

// Display the formatted data items
console.log(formattedData);
