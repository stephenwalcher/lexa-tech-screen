// Instantiate/declare variables
let expired = [],
    nonExpired = [],
    formattedData;

// Import required data
let data = require("./q1data.json");

// Look through all data items
data.forEach((item) => {
    if (item.isExpired) {
        // Add to expired array if marked as expired
        expired.push(item);

    } else {
        // Add to nonExpired array if not marked as expired
        nonExpired.push(item);
    }
});

// Concatenate the two arrays together, with nonExpired first
formattedData = nonExpired.concat(expired);

// Display the formatted data items
console.log(formattedData);
