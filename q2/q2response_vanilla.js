// Import Lodash package
const _ = require("lodash");

let data,
    formattedData = {},
    tabs = 0;

// Import required data
try {
    data = require("./q2data.json");

} catch (error) {
    console.error("Could not find required data file!");
    process.exit(1);
}

// Define the recursive function to generate the tree object
let createTreeObject = (root, string) => {
    // Split the dot-notation string into an array
    let keys = string.split(".");

    // Define the parent variable as the root node
    let parent = root;

    // Loop through the generated key array
    keys.forEach((key) => {
        // Check if the key already exists
        if (key in parent) {
            // Key already exists
            // Set the key as the new parent
            parent = parent[key];

        } else {
            // Key doesn't already exist
            // Create the key and set it as the new parent
            parent = parent[key] = {};
        }
    });
}

// Define the recursive function to print out the tree structure
let printObj = (obj, key) => {
    // Print the item's key with N number of tabs preceeding
    process.stdout.write(`${"\t".repeat(tabs)}${key}\n`);

    // If there are any keys (children) in this object, run the function again
    if (Object.keys(obj).length > 0) {
        // Increase the tab count for the children
        tabs++;

        // Loop through and call the function for each new child
        for (newKey in obj) {
            let newObj = obj[newKey];

            printObj(newObj, newKey);
        };

        // Decrease the tab count back down
        tabs--;
    }
}

// If the data exists, look through all the items
data.forEach((item) => {
    createTreeObject(formattedData, item);
});

// Loop through the formatted data object and run each item through the print function
for (key in formattedData) {
    let item = formattedData[key];

    printObj(item, key);

    // Reset the tab count for the root
    tabs = 0;
}
