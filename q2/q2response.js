// Import Lodash package
const _ = require("lodash");

// Instantiate/declare variables
let data,
    formattedData = {}
    tabs = 0;

// Import required data
try {
    data = require("./q2data.json");

} catch (error) {
    console.error("Could not find required data file!");
    process.exit(1);
}

// Define the recursive function to print out the tree structure
let printObj = (obj, key) => {
    // Print the item's key with N number of tabs preceeding
    process.stdout.write(`${"\t".repeat(tabs)}${key}\n`);

    // If there are any keys (children) in this object, run the function again
    if (Object.keys(obj).length > 0) {
        // Increase the tab count for the children
        tabs++;

        // Loop through and call the function for each new child
        _.forEach(obj, (newObj, newKey) => {
            printObj(newObj, newKey);
        });

        // Decrease the tab count back down
        tabs--;
    }
}

// If the data exists, look through all the items
_.forEach(data, (item) => {
    _.set(formattedData, item, {});
});

// Loop through the formatted data object and run each item through the print function
_.forEach(formattedData, (item, key) => {
    printObj(item, key);

    // Reset the tab count for the root
    tabs = 0;
});
