// Import Lodash package
const _ = require("lodash");

// Instantiate/declare variables
let data,
    matchedItems = [],
    requestedName = process.argv[2];

// Check and see if an argument was provided
if (requestedName === undefined) {
    console.error("Please enter an argument in order to use this script");
    process.exit(1);
}

// Import required data
try {
    data = require("./q3data.json");

} catch (error) {
    console.error("Could not find required data file!");
    process.exit(1);
}

matchedItems = _.filter(data, (item) => {
    return item.indexOf(requestedName) !== -1;
});

console.log(matchedItems);
